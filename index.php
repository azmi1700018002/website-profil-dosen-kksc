<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>KKSC UAD</title>
	<link rel="stylesheet" href="assets/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="assets/homepage/homepage.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="./assets/responsive/responsive.css">
	<script src="assets/jquery/jquery-3.4.1.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-biru">
		<div class="container">
			<a class="navbar-brand" href="index.php">KKSC UAD</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item my-2 my-lg-0">
						<a href="alldosen"><button class="btn text-light btn-link">Daftar Dosen</button></a>
					</li>
					<li class="nav-item my-2 my-lg-0">
						<a href="login/"><button class="btn btn-light ml-sm-2 login">Sign In</button></a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="jumbotron jumbotron-fluid bg-uad">
		<div class="overlay"></div>
		<div class="container form-group isi">
			<label for="">Search or Browse Profiles</label>
			<form class="cari form-inline" method="get" action="cari.php">
				<input class="form-control pr-sm-0" name="cari" type="text" placeholder="Search" aria-label="Search">
				<button class="btn" type="submit"><i class="fa fa-search"></i></button>
			</form>
		</div>
	</div>

	<!-- <div class="gambargede form-group">
		<div class="isi">
			<label for="">Search or Browse Profiles</label>
			<form class="cari form-inline">
				<input class="form-control pr-sm-0" type="text" placeholder="Search" aria-label="Search">
				<button class="btn" type="submit"><i class="fa fa-search"></i></button>
			</form>
		</div>
	</div> -->

	<div class="container footer" style="padding-left: 10px;padding-right: 10px">
		<div class="row">
			<div class="col-lg-4">
				<h5 class="judul">About KKSC UAD</h5>
				<p>Search for faculty, staff, students and postdocs in one place to find information (Expertise, Bio, Research, Publications, and more) on people you would like to collaborate with and contact information to be able to communicate with them directly. Search for Staff to also find information on current roles, professional experience, and project work. Browse a list of people affiliated with an organization by selecting from the available menu options or search for a department or organization by name.</p>
			</div>
			<div class="col-lg-4">
				<h5 class="judul">Maps</h5>
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15810.356517768967!2d110.3831212!3d-7.8332349!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x173dbeeddc56d9e!2sUniversitas%20Ahmad%20Dahlan%20-%20Kampus%204!5e0!3m2!1sid!2sid!4v1573392069037!5m2!1sid!2sid" width="auto" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
			<div class="col-lg-4">
				<h5 class="judul">Menu</h5>
				<a href="https://www.instagram.com/kksc.tif.uad/"><button class="btn btn-outline-primary" style=";margin-bottom: 10px;width: 100%"><i class="fa fa-instagram" style="margin-right: 20px"></i>Instagram</button></a><br>
				<a href="http://kksc.tif.uad.ac.id/"><button class="btn btn-outline-primary" style=";margin-bottom: 10px;width: 100%"> <i class="fa fa-globe" style="margin-right: 20px;"></i>Blog KKSC</button></a><br>
				<a href="https://goo.gl/maps/fBaEwqzEfFPz8mFh6"><button style=";margin-bottom: 10px;width: 100%" class="btn btn-outline-primary"><i class="fa fa-map" style="margin-right: 20px;"></i>Maps KKSC</button></a>

					
			</div>
		</div>
	</div>

	<footer class="credit">Copyright &#9400; 2019 KKSC UAD</footer>

	<script src="assets/bootstrap/bootstrap.min.js"></script>
</body>
</html>