<?php 
include '../database/config.php';
if (isset($_GET['id'])) {
	$id = addslashes(trim($_GET['id']));

	$query = "SELECT user.NIP as nip, user.Email as email, data_diri.foto as foto, data_diri.Nama_Lengkap as Nama, data_diri.Bio_Singkat as biosing, data_diri.Bio_Lengkap as bioleng, data_diri.Web as web, data_diri.Akademi as akademi, data_diri.Penghargaan as penghargaan, data_diri.Kontak as kontak, data_diri.Info_lain as info, makul_ajar.KelasSem as kelas, infoajar.info as infoajar
	FROM user JOIN data_diri on user.NIP = data_diri.NIP
	join makul_ajar on user.NIP = makul_ajar.NIP 
	join infoajar on user.NIP = infoajar.NIP WHERE user.NIP = '$id'";
	$ambil = mysqli_query($koneksi, $query);

	$ambiljurnal = mysqli_query($koneksi, "SELECT * FROM publication WHERE id_user = '$id'");
	$ambilreferensi= mysqli_query($koneksi, "SELECT * FROM data_software WHERE id_user = '$id'");
	$ambilresearch= mysqli_query($koneksi, "SELECT * FROM research WHERE id_user = '$id'");
	$ambilprojek= mysqli_query($koneksi, "SELECT * FROM projek WHERE id_user = '$id'");
	$ambildownload= mysqli_query($koneksi, "SELECT * FROM download WHERE id_user = '$id'");
}
else{
	header("location:../alldosen.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>KKSC UAD</title>
	<link rel="stylesheet" href="../assets/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/homepage/detail.css">
	<link rel="stylesheet" href="../assets/responsive/responsive.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<script src="../assets/jquery/jquery-3.4.1.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-biru">
		<div class="container">
			<a class="navbar-brand" href="/">KKSC UAD</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<form class="form-inline my-2 my-lg-0 cari cariah ml-auto">
					<input class="form-control pr-sm-0 mr-sm-2" type="text" placeholder="Search" aria-label="Search">
					<button class="btn" type="submit"><i class="fa fa-search"></i></button>
				</form>
				<ul class="navbar-nav ml-auto">
					<li class="nav-item my-2 my-lg-0">
						<a href="../login/"><button class="btn btn-light ml-sm-2 login">Sign In</button></a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	
	<div class="section gambargede">
		<div class="container">
			<div class="row dataatas">
				<?php 
				foreach ($ambil as $ambil) { ?>
					<div class="col-lg-2">
						<div class="image gambardetail">
							<img src="../assets/image/<?= $ambil['foto'] ?>" alt="">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="datadiri">
							<div class="Nama">
								<h5><?= $ambil['Nama'] ?></h5>
							</div>
							<div class="ket">
								<h6><?= $ambil['biosing'] ?></h6>
								Web Page : <a target="_blank" href="https://<?= $ambil['web'] ?>"><?= $ambil['web'] ?></a>
							</div>
						</div>
					</div>
					<div class="col-lg-2 ml-auto">
						<!-- <button class="btn btn-outline-warning"><i class="fa fa-print"></i> Print Profile</button> -->
						<a href="mailto:<?= $ambil['email'] ?>?subject=Feedback"><button class="btn btn-outline-warning"><i class="fa fa-envelope"></i> Kirim Email</button></a>
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</div>

	<section class="data-isi">
		<div class="container">
			<div class="row">
				<div class="col">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">BIO</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="profile-tab" data-toggle="tab" href="#teaching" role="tab" aria-controls="profile" aria-selected="false">TEACHING</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="contact-tab" data-toggle="tab" href="#datasoftware" role="tab" aria-controls="contact" aria-selected="false">PUBLICATION</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="contact-tab" data-toggle="tab" href="#referensi" role="tab" aria-controls="contact" aria-selected="false">DATA / SOFTWARE</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="contact-tab" data-toggle="tab" href="#research" role="tab" aria-controls="contact" aria-selected="false">RESEARCH</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="contact-tab" data-toggle="tab" href="#project" role="tab" aria-controls="contact" aria-selected="false">PROJECT</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="contact-tab" data-toggle="tab" href="#download" role="tab" aria-controls="contact" aria-selected="false">DOWNLOAD</a>
						</li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade" id="referensi" role="tabpanel" aria-labelledby="profile-tab">
							<div class="row">
								<div class="col-lg-12 isi">
									<h5>Data / Software</h5><br>
									<?php if (0 == mysqli_num_rows($ambilreferensi)) {
										echo "Tidak ada data";
									}else{
										$no = 1;?>
										<?php foreach ($ambilreferensi as $ambilreferensi) {
											?>
											<div class="row jurnal">
												<div class="col-lg isi">
													<h4><?= $ambilreferensi['Judul'] ?></h4>
													<p class="abstrak">Sitasi : <?= $ambilreferensi['sitasi'] ?></p>
													<p>Keterangan : <br><?= $ambilreferensi['ket'] ?></p>
													<a href="https://<?= $ambilreferensi['Link'] ?>"><button class="btn btn-primary">Download</button></a>
												</div>
											</div>
										<?php }
									} ?>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="research" role="tabpanel" aria-labelledby="profile-tab">
							<div class="row">
								<div class="col-lg-12 isi">
									<h5>Research</h5><br>
									<?php if (0 == mysqli_num_rows($ambilresearch)) {
										echo "Tidak ada data";
									}else{
										$no = 1;?>
										<?php foreach ($ambilresearch as $ambilresearch) {
											?>
											<div class="row jurnal">
												<div class="col col-lg-2 info">
													<h6>Peneliti : </h6>
													<p><?= $ambilresearch['Peneliti'] ?></p>
													<h6>Tahun :</h6>
													<p style="margin-bottom: 0px;"><?= $ambilresearch['Tahun'] ?></p>
												</div>
												<div class="col col-lg-10 isi">
													<h4><?= $ambilresearch['Judul'] ?></h4>
													<p class="abstrak">Skim Penelitian : <?= $ambilresearch['Skim_penelitian'] ?></p>
													<p>Keterangan : <br><?= $ambilresearch['ket'] ?></p>
													<a href="https://<?= $ambilresearch['Link'] ?>"><button class="btn btn-primary">Download</button></a>
												</div>
											</div>
										<?php }
									} ?>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="project" role="tabpanel" aria-labelledby="profile-tab">
							<div class="row">
								<div class="col-lg-12 isi">
									<h5>Project</h5><br>
									<?php if (0 == mysqli_num_rows($ambilprojek)) {
										echo "Tidak ada data";
									}else{
										$no = 1;?>
										<?php foreach ($ambilprojek as $ambilprojek) {
											?>
											<div class="row jurnal">
												<div class="col col-lg-2 info">
													<h6>Peneliti : </h6>
													<p><?= $ambilprojek['Peneliti'] ?></p>
													<h6>Tahun :</h6>
													<p style="margin-bottom: 0px;"><?= $ambilprojek['Tahun'] ?></p>
												</div>
												<div class="col col-lg-10 isi">
													<h4><?= $ambilprojek['Judul'] ?></h4>
													<p class="abstrak">Skim Penelitian : <?= $ambilprojek['Skim_penelitian'] ?></p>
													<p>Keterangan : <br><?= $ambilprojek['ket'] ?></p>
													<a href="https://<?= $ambilprojek['Link'] ?>"><button class="btn btn-primary">Download</button></a>
												</div>
											</div>
										<?php }
									} ?>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="download" role="tabpanel" aria-labelledby="profile-tab">
							<div class="row">
								<div class="col-lg-12 isi">
									<h5>DOWNLOAD</h5><br>
									<?php if (0 == mysqli_num_rows($ambildownload)) {
										echo "Tidak ada data";
									}else{
										$no = 1;?>
										<table class="table table-striped table-bordered">
											<thead>
												<th>No</th>
												<th>Judul</th>
											</thead>
											<tbody>
												<?php foreach ($ambildownload as $ambildownload) { ?>
													<tr>
														<td><?= $no ?></td>
														<td>
															<b><?= $ambildownload['Judul'] ?></b><br>
															<a target="_blank" style="text-transform: capitalize;" href="http://<?= $ambildownload['Link'] ?>">Download
															</a>
														</td>
													</tr>
													<?php 	$no++;
												} ?>
											</tbody>
										</table>
										<?php 
									} ?>
								</div>
							</div>
						</div>

						<div class="tab-pane fade" id="datasoftware" role="tabpanel" aria-labelledby="profile-tab">
							<div class="row">
								<div class="col-lg-12 isi">
									<h5>Publication</h5><br>
									<?php if (0 == mysqli_num_rows($ambiljurnal)) {
										echo "Tidak ada data";
									}else{
										$no = 1;?>
										<?php foreach ($ambiljurnal as $ambiljurnal) {
											?>
											<div class="row jurnal">
												<div class="col col-lg-2 info" >
													<h6>Author : </h6>
													<p><?= $ambiljurnal['Author'] ?></p>
													<h6>Tahun :</h6>
													<p style="margin-bottom: 0px;"><?= $ambiljurnal['Tahun'] ?></p>
												</div>
												<div class="col col-lg-10 isi">
													<h4><?= $ambiljurnal['Judul'] ?></h4>
													<p class="abstrak">Abstrak : <?= $ambiljurnal['Abstrak'] ?></p>
													<p>Keterangan : <br><?= $ambiljurnal['ket'] ?></p>
													<a href="https://<?= $ambiljurnal['Link'] ?>"><button class="btn btn-primary">Download</button></a>
												</div>
											</div>
										<?php }
									} ?>
								</div>
							</div>
						</div>

						<div class="tab-pane fade" id="teaching" role="tabpanel" aria-labelledby="profile-tab">
							<div class="row">
								<div class="col-lg-12 isi">
									<h5>Kelas</h5><br>
									<p><?= $ambil['kelas'] ?></p>
								</div>
							</div>
						</div>

						<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
							<div class="row">
								<div class="col-lg-8 isi isidetail">
									<h5>Bio</h5><br>
									<p><?= $ambil['bioleng'] ?></p>
									<h5>Academic</h5><br>
									<p><?= $ambil['akademi'] ?></p>
									<h5>Honors & Awards</h5><br>
									<p><?= $ambil['penghargaan'] ?></p>
								</div>
								<div class="col-lg-4 isi">
									<h5>Contact</h5><br>
									<p><?= $ambil['kontak'] ?></p>
									<h5>Info Lain</h5><br>
									<p><?= $ambil['info'] ?></p>
									<h5>Info Mengajar</h5><br>
									<p><?= $ambil['infoajar'] ?></p>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer class="credit">Copyright &#9400; 2019 KKSC UAD</footer>

	<script src="../assets/bootstrap/bootstrap.min.js"></script>
</body>
</html>