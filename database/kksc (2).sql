-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2019 at 03:05 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kksc`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_diri`
--

CREATE TABLE `data_diri` (
  `id_data` int(11) NOT NULL,
  `foto` varchar(1000) NOT NULL,
  `Nama_Lengkap` varchar(50) NOT NULL,
  `Bio_Singkat` varchar(30) NOT NULL,
  `Bio_Lengkap` varchar(5000) NOT NULL,
  `Web` varchar(100) NOT NULL,
  `Akademi` varchar(1000) NOT NULL,
  `Penghargaan` varchar(1000) NOT NULL,
  `Kontak` varchar(500) NOT NULL,
  `Info_lain` varchar(1000) NOT NULL,
  `NIP` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_diri`
--

INSERT INTO `data_diri` (`id_data`, `foto`, `Nama_Lengkap`, `Bio_Singkat`, `Bio_Lengkap`, `Web`, `Akademi`, `Penghargaan`, `Kontak`, `Info_lain`, `NIP`) VALUES
(3, 'user.png', '231b23', '-12f3123f1f2', '-123123', '-123123', '3123122', '123123', '-3123123', '1231231d231d2', '1700018055');

-- --------------------------------------------------------

--
-- Table structure for table `data_software`
--

CREATE TABLE `data_software` (
  `id` int(10) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `ket` varchar(1000) NOT NULL,
  `Link` varchar(500) NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_software`
--

INSERT INTO `data_software` (`id`, `Judul`, `ket`, `Link`, `id_user`) VALUES
(2, 'software', '12312312', '123123', '1700018055'),
(4, 'referensi 2', '12312312', 'www.google.com', '1700018055');

-- --------------------------------------------------------

--
-- Table structure for table `download`
--

CREATE TABLE `download` (
  `id` int(10) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `Link` varchar(500) NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `download`
--

INSERT INTO `download` (`id`, `Judul`, `Link`, `id_user`) VALUES
(1, 'download', '22', '1700018055');

-- --------------------------------------------------------

--
-- Table structure for table `infoajar`
--

CREATE TABLE `infoajar` (
  `id` int(11) NOT NULL,
  `info` varchar(1000) NOT NULL,
  `NIP` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `infoajar`
--

INSERT INTO `infoajar` (`id`, `info`, `NIP`) VALUES
(1, 'Kelas A libur\r\n', '1700018055');

-- --------------------------------------------------------

--
-- Table structure for table `makul_ajar`
--

CREATE TABLE `makul_ajar` (
  `id` int(11) NOT NULL,
  `KelasSem` varchar(200) NOT NULL,
  `NIP` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `makul_ajar`
--

INSERT INTO `makul_ajar` (`id`, `KelasSem`, `NIP`) VALUES
(3, 'Kelas A informatika', '1700018055');

-- --------------------------------------------------------

--
-- Table structure for table `projek`
--

CREATE TABLE `projek` (
  `id` int(10) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `ket` varchar(1000) NOT NULL,
  `Link` varchar(500) NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projek`
--

INSERT INTO `projek` (`id`, `Judul`, `ket`, `Link`, `id_user`) VALUES
(2, 'projek 1', 'dwaeaw', 'www.google.com', '1700018055');

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE `publication` (
  `id` int(10) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `ket` varchar(1000) NOT NULL,
  `Link` varchar(500) NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publication`
--

INSERT INTO `publication` (`id`, `Judul`, `ket`, `Link`, `id_user`) VALUES
(2, 'bla bla bla', '2131c231c23', 'www.google.com', '1700018055');

-- --------------------------------------------------------

--
-- Table structure for table `research`
--

CREATE TABLE `research` (
  `id` int(10) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `ket` varchar(1000) NOT NULL,
  `Link` varchar(500) NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `research`
--

INSERT INTO `research` (`id`, `Judul`, `ket`, `Link`, `id_user`) VALUES
(1, 'research 1', '123123', 'www.google.com', '1700018055');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `NIP` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(500) NOT NULL,
  `Tugas` enum('Admin','Dosen') NOT NULL,
  `kodeverifikasi` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`NIP`, `Email`, `Password`, `Tugas`, `kodeverifikasi`) VALUES
('1700018055', 'muhammad1700018055@webmail.uad.ac.id', '4297f44b13955235245b2497399d7a93', 'Dosen', '85521'),
('admin', 'musafitriyadi@gmail.com', '4297f44b13955235245b2497399d7a93', 'Admin', '76584');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_diri`
--
ALTER TABLE `data_diri`
  ADD PRIMARY KEY (`id_data`),
  ADD KEY `NIP` (`NIP`);

--
-- Indexes for table `data_software`
--
ALTER TABLE `data_software`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `infoajar`
--
ALTER TABLE `infoajar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `NIP` (`NIP`);

--
-- Indexes for table `makul_ajar`
--
ALTER TABLE `makul_ajar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `NIP` (`NIP`);

--
-- Indexes for table `projek`
--
ALTER TABLE `projek`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `research`
--
ALTER TABLE `research`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`NIP`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_diri`
--
ALTER TABLE `data_diri`
  MODIFY `id_data` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_software`
--
ALTER TABLE `data_software`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `download`
--
ALTER TABLE `download`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `infoajar`
--
ALTER TABLE `infoajar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `makul_ajar`
--
ALTER TABLE `makul_ajar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `projek`
--
ALTER TABLE `projek`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `publication`
--
ALTER TABLE `publication`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `research`
--
ALTER TABLE `research`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_diri`
--
ALTER TABLE `data_diri`
  ADD CONSTRAINT `data_diri_ibfk_1` FOREIGN KEY (`NIP`) REFERENCES `user` (`NIP`);

--
-- Constraints for table `data_software`
--
ALTER TABLE `data_software`
  ADD CONSTRAINT `data_software_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`NIP`);

--
-- Constraints for table `download`
--
ALTER TABLE `download`
  ADD CONSTRAINT `download_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`NIP`);

--
-- Constraints for table `infoajar`
--
ALTER TABLE `infoajar`
  ADD CONSTRAINT `infoajar_ibfk_1` FOREIGN KEY (`NIP`) REFERENCES `user` (`NIP`);

--
-- Constraints for table `makul_ajar`
--
ALTER TABLE `makul_ajar`
  ADD CONSTRAINT `makul_ajar_ibfk_1` FOREIGN KEY (`NIP`) REFERENCES `user` (`NIP`);

--
-- Constraints for table `projek`
--
ALTER TABLE `projek`
  ADD CONSTRAINT `projek_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`NIP`);

--
-- Constraints for table `publication`
--
ALTER TABLE `publication`
  ADD CONSTRAINT `publication_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`NIP`);

--
-- Constraints for table `research`
--
ALTER TABLE `research`
  ADD CONSTRAINT `research_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`NIP`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
