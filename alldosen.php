<?php 
include 'database/config.php';
$ambil = mysqli_query($koneksi, "SELECT * FROM data_diri");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>KKSC UAD</title>
	<link rel="stylesheet" href="assets/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="assets/homepage/alldosen.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/responsive/responsive.css">
	<script src="assets/jquery/jquery-3.4.1.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-biru">
		<div class="container">
			<a class="navbar-brand" href="/">KKSC UAD</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<form class="form-inline my-2 my-lg-0 cari cariah ml-auto" method="get" action="cari.php">
					<input class="form-control pr-sm-0" type="text" name="cari" placeholder="Search" aria-label="Search">
					<button class="btn" type="submit"><i class="fa fa-search"></i></button>
				</form>
				<ul class="navbar-nav ml-auto">
					<li class="nav-item my-2 my-lg-0">
						<a href="login/"><button class="btn btn-light ml-sm-2 login">Sign In</button></a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container isi-dosen">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb kertas">
				<li class="breadcrumb-item"><a href="./index.php">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">ALL DOSEN</li>
			</ol>
		</nav>
		<div class="card-columns">
			<?php 
			foreach ($ambil as $ambil) { ?>
				<div class="card kertas" id="foto">
					<div class="image">
						<img src="./assets/image/<?= $ambil['foto'] ?>" alt="">
					</div>
					<div class="datadiri">
						<div class="Nama">
							<a href="detail/index.php?id=<?= $ambil['NIP'] ?>"><h5><?= $ambil['Nama_Lengkap'] ?></h5></a>
						</div>
						<div class="ket">
							<?= $ambil['Bio_Singkat'] ?>
						</div>
					</div>
				</div>

				<?php
			}
			?>
		</div>
	</div>

	<footer class="credit">Copyright &#9400; 2019 KKSC UAD</footer>

	<script src="assets/bootstrap/bootstrap.min.js"></script>
</body>
</html>