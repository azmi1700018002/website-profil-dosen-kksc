$(function(){
	// setting button dan form kelas
	$('#editfoto').click(function(){
		document.getElementById("uploadfoto").style.display = "block";
		document.getElementById("uploadfoto").style.margin = "0px 10px 0px 0px";
		document.getElementById("batalfoto").style.display = "block";
		document.getElementById("editfoto").style.display = "none";
		document.getElementById("fotoku").style.display = "block";
	});

	$('#batalfoto').click(function(){
		document.getElementById("fotoku").style.display = "none";
		document.getElementById("editfoto").style.display = "block";
		document.getElementById("uploadfoto").style.display = "none";
		document.getElementById("batalfoto").style.display = "none";
	});
	// penutup setting button dan form jurnal

	// setting button dan form data diri
	$('#edit').click(function(){
		$("#nama").attr("readonly", false); 
		$("#biosing").attr("readonly", false); 
		$("#web").attr("readonly", false); 
		$("#bioleng").attr("readonly", false); 
		$("#kontak").attr("readonly", false); 
		$("#akademi").attr("readonly", false); 
		$("#penghargaan").attr("readonly", false); 
		$("#info").attr("readonly", false); 
		$("#emaildosen").attr("readonly", false); 
		document.getElementById("update").style.display = "block";
		document.getElementById("update").style.margin = "0px 10px 0px 0px";
		document.getElementById("batal").style.display = "block";
		document.getElementById("edit").style.display = "none";
	});

	$('#batal').click(function(){
		$("#nama").attr("readonly", true); 
		$("#biosing").attr("readonly", true); 
		$("#web").attr("readonly", true); 
		$("#bioleng").attr("readonly", true); 
		$("#kontak").attr("readonly", true); 
		$("#akademi").attr("readonly", true); 
		$("#penghargaan").attr("readonly", true); 
		$("#info").attr("readonly", true); 
		$("#emaildosen").attr("readonly", true); 
		document.getElementById("edit").style.display = "block";
		document.getElementById("update").style.display = "none";
		document.getElementById("batal").style.display = "none";
	});
	// penutup setting button dan form data diri

	// setting button dan form jurnal
	$('#edit2').click(function(){
		$("#jurnal").attr("readonly", false);
		$("#referensi").attr("readonly", false);
		document.getElementById("update2").style.display = "block";
		document.getElementById("update2").style.margin = "0px 10px 0px 0px";
		document.getElementById("batal2").style.display = "block";
		document.getElementById("edit2").style.display = "none";
	});

	$('#batal2').click(function(){
		$("#jurnal").attr("readonly", true); 
		$("#referensi").attr("readonly", true); 
		document.getElementById("edit2").style.display = "block";
		document.getElementById("update2").style.display = "none";
		document.getElementById("batal2").style.display = "none";
	});
	// penutup setting button dan form jurnal

	// setting button dan form kelas
	$('#edit3').click(function(){
		$("#kelas").attr("readonly", false);
		document.getElementById("update3").style.display = "block";
		document.getElementById("update3").style.margin = "0px 10px 0px 0px";
		document.getElementById("batal3").style.display = "block";
		document.getElementById("edit3").style.display = "none";
	});

	$('#batal3').click(function(){
		$("#kelas").attr("readonly", true); 
		document.getElementById("edit3").style.display = "block";
		document.getElementById("update3").style.display = "none";
		document.getElementById("batal3").style.display = "none";
	});
	// penutup setting button dan form jurnal

	// setting button dan form kelas
	$('#edit4').click(function(){
		$("#mengajar").attr("readonly", false);
		document.getElementById("update4").style.display = "block";
		document.getElementById("update4").style.margin = "0px 10px 0px 0px";
		document.getElementById("batal4").style.display = "block";
		document.getElementById("edit4").style.display = "none";
	});

	$('#batal4').click(function(){
		$("#mengajar").attr("readonly", true); 
		document.getElementById("edit4").style.display = "block";
		document.getElementById("update4").style.display = "none";
		document.getElementById("batal4").style.display = "none";
	});
	// penutup setting button dan form jurnal

});