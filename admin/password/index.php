<?php 
include '../templates/db.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin - KKSC UAD</title>
	<?php include '../templates/header.php'; ?>
	<style>
		footer{
			bottom: 0px;
			position: absolute;
		}
	</style>
</head>
<body>
	<?php include '../templates/navbar.php'; ?>
	<?php include '../templates/notifikasi.php'; ?>
	<div class="container konten">
		<div class="row justify-content-md-center align-items-center">
			<div class="col-lg-4 kertas">
				<h5 class="judul">Ubah Password</h5>
				<form action="prosespassword.php" method="post">
					<div class="form-group">
						<label for="">Password Lama</label>
						<input type="password" class="form-control" name="passwordlama" placeholder="Password Lama" required>

					</div>
					<div class="form-group">
						<label for="">Password Baru</label>
						<input type="password" class="form-control" name="passwordbaru" placeholder="Password Baru" required>
					</div>
					<div class="form-group">
						<label for="">RePassword Baru</label>
						<input type="password" class="form-control" name="repasswordbaru" placeholder="RePassword Baru" required>
					</div>
					<button type="submit" class="btn btn-primary">Update Password</button>
					<a href="../dashboard/"><button type="button" class="btn btn-secondary">Kembali</button></a>
				</form>
			</div>
		</div>
	</div>
	
	<?php include '../templates/kaki.php'; ?>
</body>
</html>