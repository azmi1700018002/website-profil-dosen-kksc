<?php 
include '../templates/db.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin - KKSC UAD</title>
	<?php include '../templates/header.php'; ?>
</head>
<body>
	<?php include '../templates/navbar.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-lg-12 kertas" id="listdosen">
				<h5 class="judul">
					Daftar dosen terdaftar :
				</h5>
				<?php include '../proses/ambildata.php'; ?>
			</div>
			<div class="col-lg-12 kertas" id="tambahdosen">
				<?php include '../templates/notifikasi.php'; ?>
				<h5 class="judul">
					Tambah Dosen :
				</h5>
				<form action="../proses/prosestambah.php" method="post">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="exampleInputEmail1">NIP / NIY</label>
								<input type="text" name="NIP" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="NIP/NIY">
							</div>		
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="exampleInputEmail1">Email address</label>
								<input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Email">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="exampleInputPassword1">Password</label>
								<input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="exampleInputPassword1">Re-Password</label>
								<input type="password" name="repassword" class="form-control" id="exampleInputPassword1" placeholder="Password">
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
					<button type="reset" class="btn btn-outline-primary">Reset</button>
				</form>
			</div>
		</div>
	</div>
	
	<?php include '../templates/kaki.php'; ?>
</body>
</html>