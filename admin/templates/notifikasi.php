<?php 
// notifikasi update data
if (isset($_GET['update'])) {
	$update = $_GET['update'];
	if ($update ==  "berhasil") { ?>
		<script>
			$(document).ready(function () {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'success',
					title: 'Data Berhasil di Update'
				})
			});
		</script>
		<?php
	}
	else{ ?>
		<script>
			$(document).ready(function () {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'error',
					title: 'Data Gagal di Update'
				})
			});
		</script>
		<?php
	}
}
// penutup notifikasi update data

if (isset($_GET['pesan'])) {
	$pesan = $_GET['pesan'];
	if ($pesan == "namakosong") {?>
		<div class="alert alert-danger alert-dismissible fade show" role="alert">
			<strong>NIP/NIY Kosong!</strong> isi form nama dengan benar.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>

		<?php 
	}
	else if ($pesan == "emailkosong") {?>
		<div class="alert alert-danger alert-dismissible fade show" role="alert">
			<strong>Email Kosong!</strong> isi form email dengan benar.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	<?php
	}
	else if ($pesan == "passwordkosong") {?>
		<div class="alert alert-danger alert-dismissible fade show" role="alert">
			<strong>Password Kosong!</strong> isi form password dengan benar.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	<?php
	}
	else if ($pesan == "passwordbeda") {?>
		<div class="alert alert-danger alert-dismissible fade show" role="alert">
			<strong>Password Beda!</strong> isi form password dan Re-Password dengan isian sama.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	<?php }
}
?>

<?php 
// notifikasi update data
if (isset($_GET['kirim'])) {
	$update = $_GET['kirim'];
	if ($update ==  "berhasil") { ?>
		<script>
			$(document).ready(function () {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'success',
					title: 'Data Berhasil di Tambah'
				})
			});
		</script>
		<?php
	}
	else{ ?>
		<script>
			$(document).ready(function () {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'error',
					title: 'Data Gagal di Tambah'
				})
			});
		</script>
		<?php
	}
}
// penutup notifikasi update data

if (isset($_GET['hapus'])) {
	$update = $_GET['hapus'];
	if ($update ==  "berhasil") { ?>
		<script>
			$(document).ready(function () {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'success',
					title: 'Data Berhasil di Hapus'
				})
			});
		</script>
		<?php
	}
	else{ ?>
		<script>
			$(document).ready(function () {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'error',
					title: 'Data Gagal di Hapus'
				})
			});
		</script>
		<?php
	}
}

?>