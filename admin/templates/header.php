    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../assets/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/admin/css/admin.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../assets/sweetalert/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="../../assets/datatables/jquery.dataTables.min.css">
    <script src="../../assets/jquery/jquery-3.4.1.min.js"></script>