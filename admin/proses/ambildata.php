<?php 
$query = "SELECT user.NIP as NIP, user.Email as email, data_diri.Nama_Lengkap as nama FROM user
JOIN data_diri ON data_diri.NIP = user.NIP WHERE user.Tugas = 'Dosen'";
$ambil = mysqli_query($koneksi, $query);
$i=1;
?>

<div class="table-responsive">
	<table class="table table-striped data">
		<thead class="thead-dark">
			<tr>
				<th>No</th>
				<th>NIP</th>
				<th>Nama</th>
				<th>Email</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($ambil as $ambil) { ?>
				<tr>
					<td><?= $i ?></td>
					<td><?= $ambil['NIP'] ?></td>
					<td><?= $ambil['nama'] ?></td>
					<td><?= $ambil['email'] ?></td>
					<td>
						<a href="../proses/hapus.php?id=<?= $ambil['NIP'] ?>">
							<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete">
								<i class="fa fa-trash"></i>
							</button>
						</a>

					</td>
				</tr>
				<?php 
				$i+=1;
			} ?>
		</tbody>
	</table>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.data').DataTable();
	});
</script>