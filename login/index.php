<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pesan</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="../assets/bootstrap/bootstrap.min.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="../assets/login/login.css">
	<link rel="stylesheet" href="../assets/sweetalert/sweetalert2.min.css">
	<script src="../assets/jquery/jquery-3.4.1.min.js"></script>
</head>
<body>

	<?php 
	// notifikasi update data
	if (isset($_GET['pesan'])) {
		$update = $_GET['pesan'];
		if ($update ==  "keluar") { ?>
			<script>
				$(document).ready(function () {
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000
					});

					Toast.fire({
						type: 'success',
						title: 'Anda berhasil Logout'
					})
				});
			</script>
			<?php
		}
		else{ ?>
			<script>
				$(document).ready(function () {
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000
					});

					Toast.fire({
						type: 'error',
						title: 'Username & Password Salah'
					})
				});
			</script>
			<?php
		}
	}
	?>

	<div class="wrapper fadeInDown">
		<div id="formContent">
			<!-- Tabs Titles -->
			<h2 class="active">Login</h2>

			<!-- Login Form -->
			<form method="post" action="proses.php">
				<div class="form-group input-group mb-2">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="fa fa-user"></i></div>
					</div>
					<input type="text" id="login" class="fadeIn form-control" name="username" placeholder="Username">
				</div>
				<div class="form-group input-group">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="fa fa-key"></i></div>
					</div>
					<input type="password" id="password" class="fadeIn form-control" name="password" placeholder="Password">
					
				</div>
				<input type="submit" class="mt-sm-2 btn btn-primary fadeIn fourth" value="Masuk">
				<a href="../"><input type="button" class="mt-sm-2 btn btn-outline-primary fadeIn fourth" value="Kembali"></a>
			</form>

			<!-- Remind Passowrd -->
			<div id="formFooter">
				<p>Copyright KKSC UAD</p>
			</div>

		</div>
	</div>
	
	<script src="../assets/sweetalert/sweetalert2.min.js"></script>
	<script src="../assets/bootstrap/bootstrap.min.js"></script>
</body>
</html>