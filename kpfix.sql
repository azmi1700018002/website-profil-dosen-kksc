-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Des 2019 pada 03.36
-- Versi server: 10.1.30-MariaDB
-- Versi PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kpfix`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_diri`
--

CREATE TABLE `data_diri` (
  `id_data` int(11) NOT NULL,
  `foto` varchar(1000) NOT NULL,
  `Nama_Lengkap` varchar(50) NOT NULL,
  `Bio_Singkat` varchar(30) NOT NULL,
  `Bio_Lengkap` varchar(5000) NOT NULL,
  `Web` varchar(100) NOT NULL,
  `Akademi` varchar(1000) NOT NULL,
  `Penghargaan` varchar(1000) NOT NULL,
  `Kontak` varchar(500) NOT NULL,
  `Info_lain` varchar(1000) NOT NULL,
  `NIP` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_diri`
--

INSERT INTO `data_diri` (`id_data`, `foto`, `Nama_Lengkap`, `Bio_Singkat`, `Bio_Lengkap`, `Web`, `Akademi`, `Penghargaan`, `Kontak`, `Info_lain`, `NIP`) VALUES
(4, '60040496.jpg', 'Murinto, S.Si., M.Kom', 'Assistant Professor', 'NIDN : 510077302\r<br>NIY : 60040496\r<br>Jabatan : Fungsional Lektor\r<br>Pendidikan Terakhir : S2 Ilmu Komputer UGM\r<br>Bidang Keilmuan :Computer Vision and Image Processing\r<br>Email :murintokusno@tif.uad.ac.id\r<br>ID Sinta : <a href=\"http://sinta2.ristekdikti.go.id/authors/detail?id=23001&view=overview\">23001</a>\r<br>ID Google Scholar : <a href=\"https://scholar.google.co.id/citations?user=P4bRyiMAAAAJ&hl=en\">P4bRyiMAAAAJ</a>\r<br>ID Scopus : <a href=\"https://www.scopus.com/authid/detail.uri?authorId=57163914100\">57163914100</a>\r<br>', 'murintokusno.tif.uad.ac.id', 'Ilmu Komputer, UGM, Yogyakarta, Indonesia', 'Best Paper\r<br>Best Poster', '-', 'Image Processing, Computer Vision, Data Sciences, Machine Learning', '60040496');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_software`
--

CREATE TABLE `data_software` (
  `id` int(10) NOT NULL,
  `Judul` varchar(1000) NOT NULL,
  `ket` varchar(1000) NOT NULL,
  `Link` varchar(1000) NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_software`
--

INSERT INTO `data_software` (`id`, `Judul`, `ket`, `Link`, `id_user`) VALUES
(2, 'software', '12312312', '123123', '1700018055'),
(4, 'referensi 2', '12312312', 'www.google.com', '1700018055'),
(7, 'Pengolahan Citra (Image Processing)', 'â€œSet of computational techniques for analyzing, enhancing, compressing, and reconstructing images. Its main components are importing, in which an image is captured through scanning or digital photography; analysis and manipulation of the image, accomplished using various specialized software applications; and output (e.g., to a printer or monitor). Image processing has extensive applications in many areas, including astronomy, medicine, industrial robotics, and remote sensing by satellites â€', 'encyclopedia2.thefreedictionary.com', '60040496');

-- --------------------------------------------------------

--
-- Struktur dari tabel `download`
--

CREATE TABLE `download` (
  `id` int(10) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `Link` varchar(500) NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `download`
--

INSERT INTO `download` (`id`, `Judul`, `Link`, `id_user`) VALUES
(3, 'Aplikasi Pengenalan Citra Rambu Lalu Lintas Berbentuk Lingkaran Menggunakan Metode Jarak City-block', 'media.neliti.com/media/publications/211507-none.pdf', '60040496');

-- --------------------------------------------------------

--
-- Struktur dari tabel `infoajar`
--

CREATE TABLE `infoajar` (
  `id` int(11) NOT NULL,
  `info` varchar(1000) NOT NULL,
  `NIP` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `infoajar`
--

INSERT INTO `infoajar` (`id`, `info`, `NIP`) VALUES
(2, 'Kelas A Libur', '60040496');

-- --------------------------------------------------------

--
-- Struktur dari tabel `makul_ajar`
--

CREATE TABLE `makul_ajar` (
  `id` int(11) NOT NULL,
  `KelasSem` varchar(200) NOT NULL,
  `NIP` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `makul_ajar`
--

INSERT INTO `makul_ajar` (`id`, `KelasSem`, `NIP`) VALUES
(4, 'Kalkulus Informatika\r<br>Grafika Komputer\r<br>Pengolahan Citra\r<br>Computer Vision', '60040496');

-- --------------------------------------------------------

--
-- Struktur dari tabel `projek`
--

CREATE TABLE `projek` (
  `id` int(10) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `ket` varchar(1000) NOT NULL,
  `Link` varchar(500) NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `projek`
--

INSERT INTO `projek` (`id`, `Judul`, `ket`, `Link`, `id_user`) VALUES
(2, 'Image Segmentation Using Modification Particle Swarm Optimization', 'Goal: Algorithms', 'researchgate.net/project/Image-Segmentation-Using-Modification-Particle-Swarm-Optimization', '60040496');

-- --------------------------------------------------------

--
-- Struktur dari tabel `publication`
--

CREATE TABLE `publication` (
  `id` int(10) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `ket` varchar(1000) NOT NULL,
  `Link` varchar(500) NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `publication`
--

INSERT INTO `publication` (`id`, `Judul`, `ket`, `Link`, `id_user`) VALUES
(4, 'Dimensionality Reduction using Hybrid Support Vector Machine and Discriminant Independent Component ', 'Murinto and Nur Rochmah Dyah PA, â€œDimensionality Reduction using Hybrid Support Vector Machine and Discriminant Independent Component Analysis for Hyperspectral Imageâ€ International Journal of Advanced Computer Science and Applications(IJACSA), 8(11), 2017.', 'dx.doi.org/10.14569/IJACSA.2017.081176', '60040496');

-- --------------------------------------------------------

--
-- Struktur dari tabel `research`
--

CREATE TABLE `research` (
  `id` int(10) NOT NULL,
  `Judul` varchar(1000) NOT NULL,
  `ket` varchar(1000) NOT NULL,
  `Link` varchar(9999) NOT NULL,
  `id_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `research`
--

INSERT INTO `research` (`id`, `Judul`, `ket`, `Link`, `id_user`) VALUES
(7, 'Hyperspectral Image Classification based on Dimensionality Reduction and Swarm Optimization Approach', 'International Journal of Computer Applications', 'researchgate.net/publication/333116909_Hyperspectral_Image_Classification_based_on_Dimensionality_Reduction_and_Swarm_Optimization_Approach/link/5cea8922458515712ec3c01c/download', '60040496');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `NIP` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(500) NOT NULL,
  `Tugas` enum('Admin','Dosen') NOT NULL,
  `kodeverifikasi` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`NIP`, `Email`, `Password`, `Tugas`, `kodeverifikasi`) VALUES
('1700018055', 'muhammad1700018055@webmail.uad.ac.id', '4297f44b13955235245b2497399d7a93', 'Dosen', '85521'),
('60040496', 'murintokusno@tif.uad.ac.id', '4297f44b13955235245b2497399d7a93', 'Dosen', '99231'),
('admin', 'azmibz23@gmail.com', '4297f44b13955235245b2497399d7a93', 'Admin', '76584');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_diri`
--
ALTER TABLE `data_diri`
  ADD PRIMARY KEY (`id_data`),
  ADD KEY `NIP` (`NIP`);

--
-- Indeks untuk tabel `data_software`
--
ALTER TABLE `data_software`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `infoajar`
--
ALTER TABLE `infoajar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `NIP` (`NIP`);

--
-- Indeks untuk tabel `makul_ajar`
--
ALTER TABLE `makul_ajar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `NIP` (`NIP`);

--
-- Indeks untuk tabel `projek`
--
ALTER TABLE `projek`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `research`
--
ALTER TABLE `research`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`NIP`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_diri`
--
ALTER TABLE `data_diri`
  MODIFY `id_data` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `data_software`
--
ALTER TABLE `data_software`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `download`
--
ALTER TABLE `download`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `infoajar`
--
ALTER TABLE `infoajar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `makul_ajar`
--
ALTER TABLE `makul_ajar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `projek`
--
ALTER TABLE `projek`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `publication`
--
ALTER TABLE `publication`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `research`
--
ALTER TABLE `research`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `data_diri`
--
ALTER TABLE `data_diri`
  ADD CONSTRAINT `data_diri_ibfk_1` FOREIGN KEY (`NIP`) REFERENCES `user` (`NIP`);

--
-- Ketidakleluasaan untuk tabel `data_software`
--
ALTER TABLE `data_software`
  ADD CONSTRAINT `data_software_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`NIP`);

--
-- Ketidakleluasaan untuk tabel `download`
--
ALTER TABLE `download`
  ADD CONSTRAINT `download_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`NIP`);

--
-- Ketidakleluasaan untuk tabel `infoajar`
--
ALTER TABLE `infoajar`
  ADD CONSTRAINT `infoajar_ibfk_1` FOREIGN KEY (`NIP`) REFERENCES `user` (`NIP`);

--
-- Ketidakleluasaan untuk tabel `makul_ajar`
--
ALTER TABLE `makul_ajar`
  ADD CONSTRAINT `makul_ajar_ibfk_1` FOREIGN KEY (`NIP`) REFERENCES `user` (`NIP`);

--
-- Ketidakleluasaan untuk tabel `projek`
--
ALTER TABLE `projek`
  ADD CONSTRAINT `projek_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`NIP`);

--
-- Ketidakleluasaan untuk tabel `publication`
--
ALTER TABLE `publication`
  ADD CONSTRAINT `publication_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`NIP`);

--
-- Ketidakleluasaan untuk tabel `research`
--
ALTER TABLE `research`
  ADD CONSTRAINT `research_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`NIP`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
