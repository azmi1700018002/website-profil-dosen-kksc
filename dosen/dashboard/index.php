<?php
include '../templates/db.php';
include 'ambildata.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dosen - KKSC</title>
	<?php 
	include '../templates/header.php'; 
	include '../templates/notifikasi.php';
	?>
</head>
<body>
	<?php include '../templates/navbar.php'; ?>

	<div class="container">
		<div class="row" id="data-dosen">
			<div class="col-lg-12">
				<div class="judulke w-100">
					<h4 class="mb-0">Data Dosen</h4>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-4">
				<div class="card-columns card-foto">
					<div class="card kertas" id="foto">
						<h5 class="judul">Foto Profile</h5>
						<div class="alert alert-danger" role="alert">
							Maksimal size image 2 mb
						</div>
						<div class="image">
							<img src="../../assets/image/<?= $foto ?>" alt="">
						</div>
						<form action="proses/foto.php" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<input type="file" style="display: none" name="foto" class="form-control-file" id="fotoku">
							</div>
							<div class="form-inline">
								<button type="button" style="margin: 0px auto" id="editfoto" class="btn btn-success">Ganti Foto</button>
								<button type="submit" id="uploadfoto" style="display: none" class="btn btn-primary">Upload</button>
								<button type="button" id="batalfoto" style="display: none" class="btn btn-outline-primary">Batal</button>
							</div>
						</form>
					</div>

					<div class="card kertas">
						<h5 class="judul">Info Kelas Ampu</h5>
						<form action="proses/makul.php" method="post">
							<div class="form-group">
								<label for="">Mengajar di</label>
								<textarea name="kelas" class="form-control" id="kelas" placeholder="Kelas A Informatika 17, Kelas B Informatika 15" rows="3" readonly><?= $kelas ?></textarea>
							</div>
							<div class="form-inline">
								<button class="btn btn-success edit" type="button" id="edit3">Edit</button>
								<button class="btn btn-primary update" style="display: none" type="submit" id="update3">Update</button>
								<button class="btn btn-outline-primary update" style="display: none" type="button" id="batal3">Batal</button>
							</div>
						</form>
					</div>

					<div class="card kertas">
						<h5 class="judul">Jadwal Mengajar</h5>
						<form action="proses/mengajar.php" method="post">
							<div class="form-group">
								<label for="">Jadwal Mengajar</label>
								<textarea name="mengajar" class="form-control" id="mengajar" placeholder="Kelas A libur" rows="3" readonly><?= $info ?></textarea>
							</div>
							<div class="form-inline">
								<button class="btn btn-success edit" type="button" id="edit4">Edit</button>
								<button class="btn btn-primary update" style="display: none" type="submit" id="update4">Update</button>
								<button class="btn btn-outline-primary update" style="display: none" type="button" id="batal4">Batal</button>
							</div>
						</form>
					</div>

				</div>


			</div>

			<div class="col-lg-8">
				<div class="card-columns card-datadiri">
					<!-- card data diri -->
					<div class="card kertas" id="data-diri">
						<h5 class="judul">Data Diri</h5>
						<form action="proses/datadiri.php" method="post">
							<div class="form-row">
								<div class="form-group col-lg-6">
									<label for="">Nama Lengkap</label>
									<input type="text" class="form-control" name="nama" id="nama" value="<?= $nama ?>" placeholder="Nama Lengkap" readonly>
								</div>

								<div class="form-group col-lg-6">
									<label>Kontak</label>
									<input class="form-control" name="kontak" value="<?= $kontak ?>" id="kontak" placeholder="Kontak" readonly>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-lg-6">
									<label>Bio Singkat</label>
									<input type="text" class="form-control" name="biosing" id="biosing" value="<?= $biosing ?>" placeholder="Bio Singkat" readonly>
								</div>
								<div class="form-group col-lg-6">
									<label>Web</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">https://</div>
										</div>
										<input type="text" class="form-control" name="web" id="web" value="<?= $web ?>" placeholder="www.google.com" readonly>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Email</label>
								<input type="text" class="form-control" name="email" id="emaildosen"value="<?= $email ?>" readonly>
							</div>
							<div class="form-group">
								<label>Bio Lengkap</label>
								<textarea class="form-control" name="bioleng" id="bioleng" placeholder="Bio Lengkap" rows="10" readonly><?= $bioleng ?></textarea>
							</div>
							<div class="form-group">
								<label>Akademi</label>
								<textarea class="form-control" name="akademi" id="akademi" placeholder="Akademi" rows="3" readonly><?= $akademi ?></textarea>
							</div>
							<div class="form-group">
								<label>Penghargaan</label>
								<textarea class="form-control" name="penghargaan" id="penghargaan" placeholder="Penghargaan" rows="3" readonly><?= $penghargaan ?></textarea>
							</div>
							<div class="form-group">
								<label>Info lain</label>
								<textarea name="infoheh" class="form-control" id="info" placeholder="Penghargaan" rows="3" readonly><?= $infoheh ?></textarea>
							</div>
							<div class="form-inline">
								<button class="btn btn-success edit" type="button" id="edit">Edit</button>
								<button class="btn btn-primary update" style="display: none" type="submit" id="update">Update</button>
								<button class="btn btn-outline-primary update" style="display: none" type="button" id="batal">Batal</button>

							</div>
						</form>
						<!-- selesai card data diri -->
					</div>
					<!-- end card data diri -->
				</div>
			</div>
		</div>

		
		<div class="row" id="publish">
			<div class="col-lg-12">
				<div class="judulke w-100 mt-10">
					<h4 class="mb-0">Publication</h4>
				</div>
			</div>
		</div>
		<div class="row" >
			<div class="col-lg-12">
				<div class="card-columns card-publish">

					<div class="card kertas">
						<h5 class="judul">Publication</h5>
						<div class="form-group">
							<?php 
							if ( 0 == mysqli_num_rows($ambiljurnal)) { ?>
								<textarea class="form-control" id="kelas" rows="3" readonly>Tidak ada data</textarea>
							<?php }
							else{?>
								<table class="table" border="1" cellpadding="5" width="100%">
									<tr class="thead-dark">
										<th>Judul</th>
										<th>Link</th>
										<th>Aksi</th>
									</tr>
									<?php foreach ($ambiljurnal as $ambilahjurnal) { 
										$idJurnal = $ambilahjurnal['id'];
										$judulJurnal = $ambilahjurnal['Judul'];
										$authorJurnal = $ambilahjurnal['Author'];
										$tahunJurnal = $ambilahjurnal['Tahun'];
										$abstrakJurnal = $ambilahjurnal['Abstrak'];
										$ketJurnal = $ambilahjurnal['ket'];
										$linkJurnal = $ambilahjurnal['Link'];?>
										<tr>
											<td><?= $judulJurnal ?></td>
											<td><?= $linkJurnal ?></td>
											<td><a href="proses/hapusjurnal.php?id=<?= $idJurnal ?>"><button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></a>

												<button class="btn btn-primary updatejurnal" type="button" data-toggle="modal" data-target="#UpdateJurnal" judul="<?=$judulJurnal ?>" author="<?=$authorJurnal ?>" tahun="<?=$tahunJurnal ?>" abstrak="<?=$abstrakJurnal ?>" Link="<?=$linkJurnal ?>" ket="<?=$ketJurnal ?>" >Update</button>
											</td>
										</tr>
									<?php } ?>
								</table>
							<?php }
							?>
						</div>
						<div class="form-inline">
							<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#tambahJurnal">Tambah Publication</button>
						</div>


						<!-- Modal -->
						<script type="text/javascript">
							
							$(".updatejurnal").click(function(){
								var judul=$(this).attr("judul");
								$("#editjuduljurnal").val(judul);
								var author=$(this).attr("author");
								$("#editauthorjurnal").val(author);
								var tahun=$(this).attr("tahun");
								$("#edittahunjurnal").val(tahun);
								var abstrak=$(this).attr("abstrak");
								$("#editabstrakjurnal").val(abstrak);
								var link=$(this).attr("link");
								$("#editlinkjurnal").val(link);
								var ket=$(this).attr("ket");
								$("#editketjurnal").val(ket);
							});
						</script>
						<div class="modal fade" id="UpdateJurnal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<form action="./proses/updatejurnal.php?id=<?= $idJurnal ?>" method="post">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Update Publication</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<input type="text" class="form-control mb-3" id="editjuduljurnal" placeholder="Judul" name="judul">

											<input type="text" class="form-control mb-3" id="editauthorjurnal" placeholder="Author" name="author">

											<input type="year" class="form-control mb-3" id="edittahunjurnal" placeholder="Tahun" name="tahun">

											<input type="year" class="form-control mb-3" id="editabstrakjurnal" placeholder="abstrak" name="abstrak">

											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<div class="input-group-text">https://</div>
												</div>
												<input type="text" class="form-control" id="editlinkjurnal" placeholder="Link Jurnal" name="link">
											</div>
											<div class="input-group">
												<textarea name="ket" id="editketjurnal" placeholder="Keterangan" class="form-control" cols="30" rows="10"></textarea>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</div>

						<!-- Modal -->
						<div class="modal fade" id="tambahJurnal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<form action="./proses/tambahJurnal.php" method="post">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Tambah Publication</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<input type="text" class="form-control mb-3" placeholder="Judul" name="judul">

											<input type="text" class="form-control mb-3" placeholder="Author" name="author">

											<input type="text" pattern="\d*" maxlength="4" class="form-control mb-3" placeholder="Tahun" name="tahun">

											<input type="text" class="form-control mb-3" placeholder="Abstrak" name="abstrak">

											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<div class="input-group-text">https://</div>
												</div>
												<input type="text" class="form-control" placeholder="Link Download" name="link">
											</div>
											<div class="input-group">
												<textarea name="ket" id="ket" placeholder="Keterangan" class="form-control" cols="30" rows="10"></textarea>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="card kertas">
						<h5 class="judul">Research</h5>
						<div class="form-group">
							<?php 
							if ( 0 == mysqli_num_rows($ambilResearch)) { ?>
								<textarea class="form-control" id="kelas" rows="3" readonly>Tidak ada data</textarea>
							<?php }
							else{?>
								<table border="1" class="table" width="100%">
									<tr class="thead-dark">
										<th>Judul</th>
										<th>Link</th>
										<th>Aksi</th>
									</tr>
									<?php foreach ($ambilResearch as $ambilahResearch) {
										$idResearch = $ambilahResearch['id'];
										$judulResearch = $ambilahResearch['Judul'];

										$penelitiResearch = $ambilahResearch['Peneliti'];
										$skimResearch = $ambilahResearch['Skim_penelitian'];
										$tahunResearch = $ambilahResearch['Tahun'];

										$ketResearch = $ambilahResearch['ket'];
										$linkResearch = $ambilahResearch['Link'];?>
										<tr>
											<td><?= $judulResearch ?></td>
											<td><?= $linkResearch ?></td>
											<td><a href="proses/hapusreserch.php?id=<?= $idResearch ?>"><button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></a>

												<button class="btn btn-primary updateresearch" type="button" data-toggle="modal" data-target="#UpdateResearch" judul="<?=$judulResearch ?>" peneliti="<?=$penelitiResearch ?>" skim="<?=$skimResearch ?>" tahun="<?=$tahunResearch ?>" Link="<?=$linkResearch ?>" ket="<?=$ketResearch ?>" >Update</button>
											</td>
										</tr>
									<?php } ?>
								</table>
							<?php }
							?>
						</div>
						<div class="form-inline">
							<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#tambahResearch">Tambah Research</button>
						</div>


						<!-- Modal -->
						<script type="text/javascript">
							
							$(".updateresearch").click(function(){
								var judul=$(this).attr("judul");
								$("#editjudulresearch").val(judul);

								var peneliti=$(this).attr("peneliti");
								$("#editpenelitiresearch").val(peneliti);

								var skim=$(this).attr("skim");
								$("#editskimresearch").val(skim);
								var tahun=$(this).attr("tahun");
								$("#edittahunresearch").val(tahun);

								var link=$(this).attr("link");
								$("#editlinkresearch").val(link);
								var ket=$(this).attr("ket");
								$("#editketresearch").val(ket);
							});
						</script>
						<div class="modal fade" id="UpdateResearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<form action="./proses/updateresearch.php?id=<?=$idResearch ?>" method="post">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Update Research</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<input type="text" class="form-control mb-3" id="editjudulresearch" placeholder="Judul" name="judul">

											<input type="text" class="form-control mb-3" id="editpenelitiresearch" placeholder="Peneliti" name="peneliti">
											<input type="text" class="form-control mb-3" id="editskimresearch" placeholder="Skim" name="skim">
											<input type="text"  pattern="\d*" maxlength="4" class="form-control mb-3" id="edittahunresearch" placeholder="Tahun" name="tahun">

											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<div class="input-group-text">https://</div>
												</div>
												<input type="text" class="form-control" id="editlinkresearch" placeholder="Link Jurnal" name="link">
											</div>

											<div class="input-group">
												<textarea name="ket" id="editketresearch" placeholder="Keterangan" class="form-control" cols="30" rows="10"></textarea>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>


					<!-- Modal -->
					<div class="modal fade" id="tambahResearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<form action="./proses/tambahresearch.php" method="post">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Tambah Research</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<input type="text" class="form-control mb-3" placeholder="Judul" name="judul">

										<input type="text" class="form-control mb-3" placeholder="Peneliti" name="peneliti">
										<input type="text" class="form-control mb-3" placeholder="Skim Penelitian" name="skim">
										<input type="text"  pattern="\d*" maxlength="4" class="form-control mb-3" placeholder="Tahun" name="tahun">

										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<div class="input-group-text">https://</div>
											</div>
											<input type="text" class="form-control" placeholder="Link Download" name="link">
										</div>

										<div class="input-group">
											<textarea name="ket" id="ket" placeholder="Keterangan" class="form-control" cols="30" rows="10"></textarea>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>
								</div>
							</form>
						</div>
					</div>

					<div class="card kertas">
						<h5 class="judul">Data Software</h5>
						<div class="form-group">
							<?php 
							if ( 0 == mysqli_num_rows($ambilReferensi)) { ?>
								<textarea class="form-control" id="kelas" rows="3" readonly>Tidak ada data</textarea>
							<?php }
							else{?>
								<table border="1" class="table" width="100%">
									<tr class="thead-dark">
										<th>Judul</th>
										<th>Link</th>
										<th>Aksi</th>
									</tr>
									<?php foreach ($ambilReferensi as $ambilahReferensi) {
										$idReferensi = $ambilahReferensi['id'];
										$judulReferensi = $ambilahReferensi['Judul'];
										$ketReferensi = $ambilahReferensi['ket'];
										$sitasiReferensi = $ambilahReferensi['sitasi'];
										$linkReferensi = $ambilahReferensi['Link']; ?>
										<tr>
											<td><?= $judulReferensi ?></td>
											<td><?= $linkReferensi ?></td>
											<td><a href="proses/hapusreferensi.php?id=<?= $idReferensi ?>"><button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></a>


												<button class="btn btn-primary updatereferensi" type="button" data-toggle="modal" data-target="#updateReferensi" judul="<?=$judulReferensi ?>" sitasi="<?=$sitasiReferensi ?>" Link="<?=$linkReferensi ?>" ket="<?=$ketReferensi ?>" >Update</button>

											</td>
										</tr>
									<?php } ?>
								</table>
							<?php }
							?>
						</div>
						<div class="form-inline">
							<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#tambahReferensi">Data Software</button>
						</div>


						<!-- Modal -->
						<script type="text/javascript">

							$(".updatereferensi").click(function(){
								var judul=$(this).attr("judul");
								$("#editjudulreferensi").val(judul);
								var link=$(this).attr("link");
								$("#editlinkreferensi").val(link);

								var sitasi=$(this).attr("sitasi");
								$("#editsitasireferensi").val(sitasi);
								var ket=$(this).attr("ket");
								$("#editketreferensi").val(ket);
							});
						</script>
						<div class="modal fade" id="updateReferensi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<form action="./proses/updatereferensi.php?id=<?= $idReferensi ?>" method="post">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Data Software</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<input type="text" class="form-control mb-3" id="editjudulreferensi" placeholder="Judul" name="judul">
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<div class="input-group-text">https://</div>
												</div>
												<input type="text" class="form-control" id="editlinkreferensi" placeholder="Link Jurnal" name="link">
											</div>

											<div class="input-group mb-3">
												<textarea name="ket" id="editketreferensi" placeholder="Keterangan" class="form-control" cols="30" rows="10"></textarea>

											</div>

											<div class="input-group">
												<input type="text" class="form-control mb-3" id="editsitasireferensi" placeholder="Sitasi" name="sitasi">
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

					<!-- Modal -->
					<div class="modal fade" id="tambahReferensi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<form action="./proses/tambahreferensi.php" method="post">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Data Software</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<input type="text" class="form-control mb-3" placeholder="Judul" name="judul">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<div class="input-group-text">https://</div>
											</div>
											<input type="text" class="form-control" placeholder="Link Download" name="link">
										</div>

										<div class="input-group mb-3">
											<textarea name="ket" id="ket" placeholder="Keterangan" class="form-control" cols="30" rows="10"></textarea>
										</div>

										<div class="input-group">
											<input type="text" class="form-control" placeholder="Sitasi" name="sitasi">
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>
								</div>
							</form>
						</div>
					</div>

					<div class="card kertas">
						<h5 class="judul">Download</h5>
						<div class="form-group">
							<?php 
							if ( 0 == mysqli_num_rows($ambilDownload)) { ?>
								<textarea class="form-control" id="kelas" rows="3" readonly>Tidak ada data</textarea>
							<?php }
							else{?>
								<table border="1" class="table" width="100%">
									<tr class="thead-dark">
										<th>Judul</th>
										<th>Link</th>
										<th>Aksi</th>
									</tr>
									<?php foreach ($ambilDownload as $ambilahDownload) {
										$idDownload = $ambilahDownload['id'];
										$judulDownload = $ambilahDownload['Judul'];
										$linkDownload = $ambilahDownload['Link']; ?>
										<tr>
											<td><?= $judulDownload ?></td>
											<td><?= $linkDownload ?></td>
											<td><a href="proses/hapusDownload.php?id=<?= $idDownload ?>"><button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></a>

												<button class="btn btn-primary updateDownload" type="button" data-toggle="modal" data-target="#updateDownload" judul="<?=$judulDownload ?>" Link="<?=$linkDownload ?>"  >Update</button>

											</td>
										</tr>
									<?php } ?>
								</table>
							<?php }
							?>
						</div>
						<div class="form-inline">
							<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#tambahdownload">Tambah Download</button>
						</div>


						<!-- Modal -->
						<script type="text/javascript">

							$(".updateDownload").click(function(){
								var judul=$(this).attr("judul");
								$("#editjuduldownload").val(judul);
								var link=$(this).attr("link");
								$("#editlinkdownload").val(link);
							});
						</script>
						<div class="modal fade" id="updateDownload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<form action="./proses/updatedownload.php?id=<?=$idDownload ?>" method="post">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Update Download</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<input type="text" class="form-control mb-3" id="editjuduldownload" placeholder="Judul" name="judul">
											<div class="input-group">
												<div class="input-group-prepend">
													<div class="input-group-text">https://</div>
												</div>
												<input type="text" class="form-control" id="editlinkdownload" placeholder="Link Jurnal" name="link">
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</div>

						<!-- Modal -->
						<div class="modal fade" id="tambahdownload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<form action="./proses/tambahdownload.php" method="post">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Tambah Download</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<input type="text" class="form-control mb-3" placeholder="Judul" name="judul">
											<div class="input-group">
												<div class="input-group-prepend">
													<div class="input-group-text">https://</div>
												</div>
												<input type="text" class="form-control" placeholder="Link Download" name="link">
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="card kertas">
						<h5 class="judul">Projek</h5>
						<div class="form-group">
							<?php 
							if ( 0 == mysqli_num_rows($ambilProjek)) { ?>
								<textarea class="form-control" id="kelas" rows="3" readonly>Tidak ada data</textarea>
							<?php }
							else{?>
								<table border="1" class="table" width="100%">
									<tr class="thead-dark">
										<th>Judul</th>
										<th>Link</th>
										<th>Aksi</th>
									</tr>
									<?php foreach ($ambilProjek as $ambilahProjek) {
										$idProjek = $ambilahProjek['id'];
										$judulProjek = $ambilahProjek['Judul'];

										$penelitiProjek = $ambilahProjek['Peneliti'];
										$skimProjek = $ambilahProjek['Skim_penelitian'];
										$tahunProjek = $ambilahProjek['Tahun'];

										$ketProjek = $ambilahProjek['ket'];
										$linkProjek = $ambilahProjek['Link']; ?>
										<tr>
											<td><?= $judulProjek ?></td>
											<td><?= $linkProjek ?></td>
											<td><a href="proses/hapusProjek.php?id=<?= $idProjek ?>"><button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></a>

												<button class="btn btn-primary updateprojek" type="button" data-toggle="modal" data-target="#updateProjek" judul="<?=$judulProjek ?>" peneliti="<?=$penelitiProjek ?>" skim="<?=$skimProjek ?>" tahun="<?=$tahunProjek ?>" Link="<?=$linkReferensi ?>" ket="<?=$ketProjek ?>" >Update</button>
											</td>
										</tr>
									<?php } ?>
								</table>
							<?php }
							?>
						</div>
						<div class="form-inline">
							<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#tambahProjek">Tambah Projek</button>
						</div>


						<!-- Modal -->
						<script type="text/javascript">
							$(".updateprojek").click(function(){
								var judul=$(this).attr("judul");
								$("#editjudulprojek").val(judul);

								var peneliti=$(this).attr("peneliti");
								$("#editpenelitiprojek").val(peneliti);
								var skim=$(this).attr("skim");
								$("#editskimprojek").val(skim);
								var tahun=$(this).attr("tahun");
								$("#edittahunprojek").val(tahun);

								var link=$(this).attr("link");
								$("#editlinkprojek").val(link);
								var ket=$(this).attr("ket");
								$("#editketprojek").val(ket);
							});
						</script>
						<div class="modal fade" id="updateProjek" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<form action="./proses/updateProjek.php?id=<?= $idProjek ?>" method="post">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Update Projek</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<input type="text" class="form-control mb-3" id="editjudulprojek" placeholder="Judul" name="judul">

											<input type="text" class="form-control mb-3" id="editpenelitiprojek" placeholder="peneliti" name="peneliti">
											<input type="text" class="form-control mb-3" id="editskimprojek" placeholder="skim" name="skim">
											<input type="text" maxlength="4" class="form-control mb-3" id="edittahunprojek" placeholder="tahun" name="tahun">

											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<div class="input-group-text">https://</div>
												</div>
												<input type="text" class="form-control" id="editlinkprojek" placeholder="Link Jurnal" name="link">
											</div>

											<div class="input-group">
												<textarea name="ket" id="editketprojek" placeholder="Keterangan" class="form-control" cols="30" rows="10"></textarea>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal -->
				<div class="modal fade" id="tambahProjek" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<form action="./proses/tambahProjek.php" method="post">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Tambah Projek</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<input type="text" class="form-control mb-3" placeholder="Judul" name="judul">

									<input type="text" class="form-control mb-3" placeholder="Peneliti" name="peneliti">
									<input type="text" class="form-control mb-3" placeholder="Skim Penelitian" name="skim">
									<input type="text" class="form-control mb-3" placeholder="Tahun" name="Tahun">

									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<div class="input-group-text">https://</div>
										</div>
										<input type="text" class="form-control" placeholder="Link Download" name="link">
									</div>

									<div class="input-group">
										<textarea name="ket" id="ket" placeholder="Keterangan" class="form-control" cols="30" rows="10"></textarea>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include '../templates/kaki.php'; ?>
</body>
</html>
