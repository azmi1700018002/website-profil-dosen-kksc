<?php 
include '../../../database/config.php';
 
session_start();
 
if($_SESSION['status'] == "Admin"){
	header("location:../../admin");
}
else if($_SESSION['status'] !="Dosen"){
	header("location:../../login");
}

$username = $_SESSION['username'];
$nama = addslashes(trim($_POST['nama']));
$biosing = addslashes(trim($_POST['biosing']));
$web = addslashes(trim($_POST['web']));
$email = addslashes(trim($_POST['email']));
$bioleng = str_replace("\n", "<br>", $_POST['bioleng']);
$kontak = str_replace("\n", "<br>", $_POST['kontak']);
$akademi = str_replace("\n", "<br>", $_POST['akademi']);
$penghargaan = str_replace("\n", "<br>", $_POST['penghargaan']);
$infoheh = str_replace("\n", "<br>", $_POST['infoheh']);

$update = mysqli_query($koneksi, "UPDATE `data_diri` SET `Nama_Lengkap`='$nama',`Bio_Singkat`='$biosing',`Bio_Lengkap`='$bioleng',`Web`='$web',`Akademi`='$akademi',`Penghargaan`='$penghargaan',`Kontak`='$kontak',`Info_lain`='$infoheh' WHERE NIP = '$username'");

$updateemail = mysqli_query($koneksi, "UPDATE `user` SET Email='$email' WHERE NIP = '$username'");

if ($update) {
	header("location:../../dashboard/index.php?update=berhasil");
}
else{
	header("location:../../dashboard/index.php?update=gagal");
}