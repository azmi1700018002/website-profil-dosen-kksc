<?php 
// notifikasi update data
if (isset($_GET['update'])) {
	$update = $_GET['update'];
	if ($update ==  "berhasil") { ?>
		<script>
			$(document).ready(function () {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'success',
					title: 'Data Berhasil di Update'
				})
			});
		</script>
		<?php
	}
	else{ ?>
		<script>
			$(document).ready(function () {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'error',
					title: 'Data Gagal di Update'
				})
			});
		</script>
		<?php
	}
}
// penutup notifikasi update data

// delete notifikasi
if (isset($_GET['delete'])) {
	$delete = $_GET['delete'];
	if ($delete ==  "berhasil") { ?>
		<script>
			$(document).ready(function () {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'success',
					title: 'Data Berhasil di Hapus'
				})
			});
		</script>
		<?php
	}
	else{ ?>
		<script>
			$(document).ready(function () {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'error',
					title: 'Data Gagal di Hapus'
				})
			});
		</script>
		<?php
	}
}
// penutup delete notifikasi

// notifikasi upload foto
if (isset($_GET['foto'])) {
	$update = $_GET['foto'];
	if ($update ==  "berhasil") { ?>
		<script>
			$(document).ready(function () {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'success',
					title: 'Data Berhasil di Update'
				})
			});
		</script>
		<?php 
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
		header("Pragma: no-cache"); // HTTP 1.0.
		header("Expires: 0");
	}
	else{ ?>
		<script>
			$(document).ready(function () {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 3000
				});

				Toast.fire({
					type: 'error',
					title: 'Data Gagal di Update'
				})
			});
		</script>
		<?php
	}
}
// Penutup notifikasi upload foto
?>